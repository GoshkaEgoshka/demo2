def is_valid(value: str) -> bool:
    if len(value) != 6:
        print("Ticket number must consist 6 digits!")
        return False
    try:
        ticket_number = int(value)
    except ValueError:
        print("Input value must be a number!")
        return False
    if ticket_number < 0:
        print("Value must be > 0")
        return False
    if ticket_number > 999999:
        print("Value must be < 999999")
        return False
    return True


def input_ticket_number(inform_string: str) -> int:
    while True:
        value = input(inform_string)
        if is_valid(value):
            return int(value)


def easy_way_happy_ticket(ticket_number: int) -> bool:
    sum_right_numbers = 0
    sum_left_numbers = 0

    for i in range(6):
        if i < 3:
            sum_right_numbers = sum_right_numbers + (ticket_number % 10)
            ticket_number = int(ticket_number/10)
        else:
            sum_left_numbers = sum_left_numbers + (ticket_number % 10)
            ticket_number = int(ticket_number/10)
    return True if sum_right_numbers == sum_left_numbers else False


def hard_way_happy_ticket(ticket_number: int) -> bool:
    sum_of_odd_numbers = 0
    sum_of_even_numbers = 0

    for _ in range(6):
        current_numeric_in_ticket = ticket_number % 10
        if current_numeric_in_ticket % 2 > 0:
            sum_of_odd_numbers = sum_of_odd_numbers + current_numeric_in_ticket
        else:
            sum_of_even_numbers = sum_of_even_numbers + current_numeric_in_ticket
        ticket_number = int(ticket_number/10)
    return True if sum_of_odd_numbers == sum_of_even_numbers else False


def ticket_counter(minimal_ticket_number: int, maximal_ticket_number: int) -> tuple:
    counter_for_hard_way = 0
    counter_for_easy_way = 0

    while minimal_ticket_number != maximal_ticket_number + 1:
        if easy_way_happy_ticket(minimal_ticket_number):
            counter_for_easy_way += 1
        if hard_way_happy_ticket(minimal_ticket_number):
            counter_for_hard_way += 1
        minimal_ticket_number += 1
    return counter_for_easy_way - counter_for_hard_way, counter_for_easy_way, counter_for_hard_way


def show_result(result: int) -> None:
    if result[0] > 0:
        print(f"Easy way win! "
              f"Easy:  {result[1]} "
              f"Hard:   {result[2]}")
    elif rezalt[1] < 0:
        print(f"Hard way win! "
              f"Easy: {result[1]} "
              f"Hard: {result[2]} ")
    else:
        print(f"Equal! "
              f"Easy: {result[1]} "
              f"Hard: {result[2]} ")


if __name__ == '__main__':
    minimal_ticket_number = input_ticket_number("Please, input minimal ticket number(6 digits): ")
    maximal_ticket_number = input_ticket_number("Please, input maximal ticket number(6 digits): ")

    show_result(ticket_counter(minimal_ticket_number, maximal_ticket_number))


