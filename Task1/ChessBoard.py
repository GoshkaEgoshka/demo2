def chess_board_display(chess_board_width, chess_board_height):
    chess_board_width = int(chess_board_width/2)

    for i in range(chess_board_height):
        if i % 2 > 0:
            print(u'\u2592\u2592\u2588\u2588' * chess_board_width)
        else:
            print(u'\u2588\u2588\u2592\u2592' * chess_board_width)


def valid(chess_board_width, chess_board_height):
    try:
        chess_board_width = int(chess_board_width)
        chess_board_height = int(chess_board_height)
    except ValueError:
        print("Width and height must be numbers")
        return False

    if chess_board_width < 2 or chess_board_height < 1:
        print("Width must be > 2 and height must be > 1")
        return False

    if chess_board_width % 2 > 0:
        print("Width must be even number")
    else:
        return True


def main():
    while True:
        chess_board_width = input("Pleas, input chessboard width: ")
        chess_board_height = input("Pleas, input chessboard height: ")
        if valid(chess_board_width, chess_board_height):
            chess_board_display(int(chess_board_width), int(chess_board_width))
            break
        else:
            continue


if __name__ == '__main__':
    main()
