from abc import ABC, abstractmethod
from typing import Any


class Input(ABC):
    @abstractmethod
    def value(self) -> Any:
        pass


class SetInputValue(Input):
    def __init__(self, input_text: str):
        self._text = input_text

    def value(self) -> Any:
        return input(self._text)


class CastToInt(Input):
    def __init__(self, some_value: Any):
        self._value = some_value

    def value(self) -> Any:
        return int(self._value.value())


class InputCache(Input):
    def __init__(self, ask: Input):
        self._ask = ask
        self._data = []

    def value(self) -> Any:
        if not self._data:
            self._data.append(self._ask.value())
        return self._data[0]


class Valid(ABC):
    @abstractmethod
    def validation(self, value: Any) -> Any:
        pass


class IsInteger(Valid):
    def validation(self, value) -> bool:
        return isinstance(value, int)


class NotLessThanZero(Valid):
    def validation(self, value) -> bool:
        return value >= 0


class LessThanMillion(Valid):
    def validation(self, value: Any) -> bool:
        return value <= 999999


class AllValidation(Valid):
    def __init__(self, *rules: Valid):
        self._checks = rules

    def validation(self, value) -> bool:
        for rule in self._checks:
            if not rule.validation(value):
                print(f"{rule.__class__.__name__} is failed")
                return False
        return True


class LuckyTickets:

    def __init__(self, min, max):
        self._min_ticket_number = min
        self._max_ticket_number = max

    def easy_way_happy_ticket(self, ticket_number: int) -> bool:
        sum_right_numbers = 0
        sum_left_numbers = 0
        counter_for_middle = 0
        ticket_number = str(ticket_number)
        ticket_number = str.zfill(ticket_number, 6)

        for i in ticket_number:
            if counter_for_middle < 3:
                sum_left_numbers += int(i)
                counter_for_middle += 1
            else:
                sum_right_numbers += int(i)
        return True if sum_right_numbers == sum_left_numbers else False

    def hard_way_happy_ticket(self, ticket_number: int) -> bool:
        sum_of_odd_numbers = 0
        sum_of_even_numbers = 0

        for _ in range(6):
            current_numeric_in_ticket = ticket_number % 10
            if current_numeric_in_ticket % 2 > 0:
                sum_of_odd_numbers = sum_of_odd_numbers + current_numeric_in_ticket
            else:
                sum_of_even_numbers = sum_of_even_numbers + current_numeric_in_ticket
            ticket_number = int(ticket_number / 10)
        return True if sum_of_odd_numbers == sum_of_even_numbers else False

    def ticket_counter(self) -> tuple:
        counter_for_hard_way = 0
        counter_for_easy_way = 0

        while self._min_ticket_number != self._max_ticket_number + 1:
            if self.easy_way_happy_ticket(self._min_ticket_number):
                counter_for_easy_way += 1
            if self.hard_way_happy_ticket(self._min_ticket_number):
                counter_for_hard_way += 1
            self._min_ticket_number += 1
        return counter_for_easy_way - counter_for_hard_way, counter_for_easy_way, counter_for_hard_way


class ShowRes:
    def __init__(self, result: tuple):
        self._result = result

    def show_result(self) -> None:
        if self._result[0] > 0:
            print(f"Easy way win! "
                  f"Easy:  {self._result[1]} "
                  f"Hard:   {self._result[2]}")
        elif self._result[0] < 0:
            print(f"Hard way win! "
                  f"Easy: {self._result[1]} "
                  f"Hard: {self._result[2]} ")
        else:
            print(f"Equal! "
                  f"Easy: {self._result[1]} "
                  f"Hard: {self._result[2]} ")


if __name__ == '__main__':
    min_ticket_number = InputCache(CastToInt(SetInputValue("Pls, input min ticket value(6 digits): ")))
    max_ticket_number = InputCache(CastToInt(SetInputValue("Pls, input input max ticket value(6 digits): ")))

    if AllValidation(
                    IsInteger(),
                    NotLessThanZero(),
                    LessThanMillion()
                    ).validation(min_ticket_number.value()):
        if AllValidation(
                        IsInteger(),
                        NotLessThanZero(),
                        LessThanMillion()
                        ).validation(max_ticket_number.value()):
            ticket = LuckyTickets(min_ticket_number.value(), max_ticket_number.value())
            show = ShowRes(ticket.ticket_counter())
            show.show_result()



